<?php

namespace Drupal\ovenmedia\Event;

use Symfony\Component\EventDispatcher\Event;
use Drupal\Core\entity\EntityInterface;

/**
 * Event that is fired an outgoing Stream ends.
 */
class OvenMediaOutgoingStreamAdmissionClosingEvent extends Event {

  const CLOSING_EVENT = 'ovenmedia_outgoing_stream_admission_closing';

  /**
   * The Entity with the ovenmedia field.
   *
   * @var Drupal\Core\entity\EntityInterface
   */
  public $entity;

  /**
   * Constructs the object.
   *
   * @param Drupal\Core\entity\EntityInterface $entity
   *   The entity that holds the ovenmedia field for the stream.
   */
  public function __construct(EntityInterface $entity) {
    $this->entity = $entity;
  }

}
