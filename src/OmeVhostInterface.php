<?php

namespace Drupal\ovenmedia;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining an ovenmediaengine vhost entity type.
 */
interface OmeVhostInterface extends ConfigEntityInterface {

  /**
   * Ovenmedia Origin Server.
   */
  public function ovenmediaOrigin();

  /**
   * Ovenmedia Edge Server.
   */
  public function ovenmediaEdge();

  /**
   * Use SSL for Edge Server.
   */
  public function ovenmediaEdgeSsl();

  /**
   * Enable Signed Policy for Vhost.
   */
  public function ovenmediaPolicy();

  /**
   * Ovenmedia RTMP Port.
   */
  public function ovenmediaPortRtmp();

  /**
   * Ovenmedia SSL Port.
   */
  public function ovenmediaPortSrt();

  /**
   * Ovenmedia API Url.
   */
  public function ovenmediaApiUrl();

  /**
   * Ovenmedia LLHLS Port.
   */
  public function ovenmediaPortLlhls();

  /**
   * Ovenmedia WSS Port.
   */
  public function ovenmediaPortWss();

}
