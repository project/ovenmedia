<?php

namespace Drupal\ovenmedia\Access;

use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Access\AccessResult;
use Symfony\Component\HttpFoundation\Request;

/**
 * Checks access for OME Header.
 */
class OmeCheckHeaderAccess implements AccessInterface {

  /**
   * Checks access.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(Request $request) {
    // Check x-ome-signature Header for access to the admission webhook.
    $service = \Drupal::service('ovenmedia.service');
    if ($x_ome_signature = $request->headers->get('x-ome-signature')) {
      $signature = $service->XOmeSignature($request->getContent(), $service->getAdmissionWebhookSecret());

      if ($signature == $x_ome_signature) {
        return AccessResult::allowed();
      }
    }
    return AccessResult::forbidden();
  }

}
