<?php

namespace Drupal\ovenmedia\Plugin\views\field;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\ResultRow;

/**
 * Field handler for Ovenmedia Fields.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("ovenmedia_app")
 */
class OvenmediaAppFieldPlugin extends FieldPluginBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $plugin = new static(
      $configuration,
      $plugin_id,
      $plugin_definition
    );

    $plugin->ovenmedia = $container->get('ovenmedia.service');

    return $plugin;
  }

  /**
   * Define the available options.
   *
   * @return array
   *   Returns view options for ovenmedia field.
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['app_display'] = ['default' => 'app'];

    return $options;
  }

  /**
   * Provide the options form.
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    $options = [
      'app' => 'App Name',
      'rtmp' => 'Rtmp Url',
      'srt' => 'Srt Url',
      'edge_wss' => 'Edge WSS Url',
      'edge_llhls' => 'Edge LLHLS Url',

    ];

    $form['app_display'] = [
      '#title' => $this->t('App Display'),
      '#type' => 'select',
      '#default_value' => $this->options['app_display'],
      '#options' => $options,
    ];

    parent::buildOptionsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Leave empty to avoid the field being used in the query.
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $render = '';
    $entity = $values->_entity;

    switch ($this->options['app_display']) {
      case 'app':
        $render = $this->ovenmedia->getAppFromEntity($entity);
        break;

      case 'rtmp':
        $render = $this->ovenmedia->getStreamUrl($entity, 'rtmp');
        break;

      case 'srt':
        $render = $this->ovenmedia->getStreamUrl($entity, 'srt');
        break;

      case 'edge_wss':
        $render = $this->ovenmedia->getEdgeUrl($entity, 'wss');
        break;

      case 'edge_llhls':
        $render = $this->ovenmedia->getEdgeUrl($entity, 'llhls');
        break;
    }
    return $render;

  }

}
