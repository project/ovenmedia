<?php

namespace Drupal\ovenmedia\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'ovenmedia_default' widget.
 *
 * @FieldWidget(
 *   id = "ovenmedia_default",
 *   label = @Translation("Ovenmedia default"),
 *   field_types = {
 *     "ovenmedia"
 *   }
 * )
 */
class OvenMediaDefaultWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    // Todo: Allow override title and label.
    $element['ome_enabled'] = [
      '#title' => $this->t('Activate streaming'),
      '#type' => 'checkbox',
      '#default_value' => isset($items[$delta]->ome_enabled) ? $items[$delta]->ome_enabled : NULL,
      '#description' => $this->t('I agree to the TOS.'),
    ];
    $settings = $items[$delta]->getFieldDefinition()->getSettings();

    if (array_key_exists('ome_custom_stream_name', $settings) && $settings['ome_custom_stream_name'] == 1) {
      $element['ome_stream_name'] = [
        '#title' => $this->t('Stream Name'),
        '#type' => 'textfield',
        '#default_value' => isset($items[$delta]->ome_stream_name) ? $items[$delta]->ome_stream_name : '',
        '#description' => $this->t('Leave empty for generated Stream Name.'),
      ];
    }
    if (array_key_exists('ome_custom_stream_psk', $settings) && $settings['ome_custom_stream_psk'] == 1) {
      $element['ome_stream_psk'] = [
        '#title' => $this->t('Custom Stream Secret'),
        '#type' => 'textfield',
        '#default_value' => isset($items[$delta]->ome_stream_psk) ? $items[$delta]->ome_stream_psk : '',
        '#description' => $this->t('Leave empty for generated Secret.'),
      ];
    }
    return $element;
  }

}
