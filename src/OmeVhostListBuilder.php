<?php

namespace Drupal\ovenmedia;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides a listing of ovenmediaengine vhosts.
 */
class OmeVhostListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Label');
    $header['id'] = $this->t('Machine name');
    $header['status'] = $this->t('Status');
    $header['ovenmedia_origin'] = $this->t('Origin');
    $header['ovenmedia_port_rtmp'] = $this->t('RTMP Port');
    $header['ovenmedia_port_srt'] = $this->t('SRT Port');
    $header['ovenmedia_edge'] = $this->t('Edge');
    $header['ovenmedia_edge_ssl'] = $this->t('Edge SSL');
    $header['ovenmedia_port_llhls'] = $this->t('LLHLS Port');
    $header['ovenmedia_port_wss'] = $this->t('WebRTC Port');
    $header['ovenmedia_api_url'] = $this->t('API Url');

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\ovenmedia\OmeVhostInterface $entity */
    $row['label'] = $entity->label();
    $row['id'] = $entity->id();
    $row['status'] = $entity->status();
    $row['ovenmedia_origin'] = $entity->ovenmediaOrigin();
    $row['ovenmedia_port_rtmp'] = $entity->ovenmediaPortRtmp();
    $row['ovenmedia_port_srt'] = $entity->ovenmediaPortSrt();
    $row['ovenmedia_edge'] = $entity->ovenmediaEdge();
    $row['ovenmedia_edge_ssl'] = $entity->ovenmediaEdgeSsl();
    $row['ovenmedia_port_llhls'] = $entity->ovenmediaPortLlhls();
    $row['ovenmedia_port_wss'] = $entity->ovenmediaPortWss();
    $row['ovenmedia_api_url'] = $entity->ovenmediaApiUrl();

    $row['status'] = $entity->status() ? $this->t('Enabled') : $this->t('Disabled');
    return $row + parent::buildRow($entity);
  }

}
